package com.djlabo.footballcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Global variables
    int scoreTeamA = 0;
    int scoreTeamB = scoreTeamA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Display the given score for team A
     */
    public void displayScoreA(int scoreA) {
        TextView scoreViewA = findViewById(R.id.team_a_score);
        scoreViewA.setText(String.valueOf(scoreA));
    }

    /**
     * Display the given score for team B
     */
    public void displayScoreB(int scoreB) {
        TextView scoreViewB = findViewById(R.id.team_b_score);
        scoreViewB.setText(String.valueOf(scoreB));
    }

    /**
     * Add 6 points for team A
     *
     * @param view is needed for android:onclick
     */

    public void addSixForTeamA(View view) {
        scoreTeamA += 6;
        displayScoreA(scoreTeamA);
    }

    /**
     * Add 2 points for team A
     *
     * @param view is needed for android:onclick
     */

    public void addTwoForTeamA(View view) {
        scoreTeamA += 2;
        displayScoreA(scoreTeamA);
    }

    /**
     * Add 1 point for team A
     *
     * @param view is needed for android:onclick
     */

    public void addOneForTeamA(View view) {
        scoreTeamA += 1;
        displayScoreA(scoreTeamA);
    }

    /**
     * Add 6 points for team B
     *
     * @param view is needed for android:onclick
     */

    public void addSixForTeamB(View view) {
        scoreTeamB += 6;
        displayScoreB(scoreTeamB);
    }

    /**
     * Add 2 points for team B
     *
     * @param view is needed for android:onclick
     */

    public void addTwoForTeamB(View view) {
        scoreTeamB += 2;
        displayScoreB(scoreTeamB);
    }

    /**
     * Add 1 point for team B
     *
     * @param view is needed for android:onclick
     */
    public void addOneForTeamB(View view) {
        scoreTeamB += 1;
        displayScoreB(scoreTeamB);
    }

    /**
     * Reset the score to default value (base score)
     *
     * @param view is needed for android:onclick
     */

    public void resetScore(View view) {
        scoreTeamA = 0;
        scoreTeamB = scoreTeamA;
        displayScoreA(scoreTeamA);
        displayScoreB(scoreTeamB);
    }
}
