/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.miwok;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialSetup();
    }

    public void initialSetup() {
        final TextView[] textViews =
                new TextView[] {
                    findViewById(R.id.numbers),
                    findViewById(R.id.family),
                    findViewById(R.id.colors),
                    findViewById(R.id.phrases)
                };
        final Class[] activities =
                new Class[] {
                    NumbersActivity.class,
                    FamilyActivity.class,
                    ColorsActivity.class,
                    PhrasesActivity.class
                };

        textViews[0].setOnClickListener(
                (View view) -> startActivity(new Intent(MainActivity.this, activities[0])));
        textViews[1].setOnClickListener(
                (View view) -> startActivity(new Intent(MainActivity.this, activities[1])));
        textViews[2].setOnClickListener(
                (View view) -> startActivity(new Intent(MainActivity.this, activities[2])));
        textViews[3].setOnClickListener(
                (View view) -> startActivity(new Intent(MainActivity.this, activities[3])));
    }
}
